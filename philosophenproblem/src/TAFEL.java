public class TAFEL{

    boolean [] gabelInBenutzung;
    
    public TAFEL(int plaetze){
        gabelInBenutzung = new boolean[plaetze];
        for (int i=0; i<plaetze; i++){
            gabelInBenutzung[i] = false;
        }
    }
    
    public int links(int i){
        return i;
    }
    
    public int rechts(int i){
        if (i < gabelInBenutzung.length-1){
            return (i+1);
        }
        else{
            return 0;
        }
    }
    
    public synchronized void gabelnFreigeben(int philplatz){
        gabelInBenutzung[rechts(philplatz)] = false;
        gabelInBenutzung[links(philplatz)] = false;
        notifyAll();
    }
    
    public synchronized void gabelnAufnehmen(int philplatz){
        while (gabelInBenutzung[rechts(philplatz)] || gabelInBenutzung[links(philplatz)]){
            System.out.println("Philosoph " + philplatz + " will essen.");
            try{
                wait();
            }catch(InterruptedException ie){};
        }
        gabelInBenutzung[rechts(philplatz)] = true;
        gabelInBenutzung[links(philplatz)] = true;
    }
}

