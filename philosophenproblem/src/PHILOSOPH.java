public class PHILOSOPH implements Runnable{

    TAFEL esstisch;
    int platznr;
    
    public PHILOSOPH(TAFEL e, int p){
        esstisch = e;
        this.platznr = p;
    }
    
    public void run(){
        for (int i = 1; i<=3; i++){
            denken(platznr);
            esstisch.gabelnAufnehmen(platznr);
            essen(platznr);
            esstisch.gabelnFreigeben(platznr);
        }
    }
    
    public void denken(int platznr){
        System.out.println("Philosoph " + platznr + " denkt...");
        try{
            Thread.sleep((int)(Math.random()*500));
        }catch(InterruptedException ie){}
    }
    
    public void essen(int platznr){
        System.out.println("Philosoph " + platznr + " isst...");
        try{
            Thread.sleep((int)(Math.random()*2000));
        }catch(InterruptedException ie){}
        System.out.println("Philosoph " + platznr + " satt!");
    }
}
