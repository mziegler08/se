/* Autor:	Marco Ziegler
 * Datum:	15.07.2013 V.1.0
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GAME extends JFrame {

	private static final long serialVersionUID = 1L;
	private KREIS ball;
	private int b, h;
	//ArrayList f�r die Bricks
	private LISTE ziele;
	private VIERECK []v;
	private VIERECK paddle;
	// gedr�ckte Tasten (keylistener)
	private boolean links, rechts, neu;
	//Thread f�r die Game-Loop
	private Thread t;
	//Wdh. pro Sec. 1000 / 25
	private static final long FRAME_PERIOD = 40;
	private static long time;
	private String ti;

	private Rectangle r1, r2;//Ball und Paddle
	private Rectangle bricks[];

	private JPanel panel;
	private BufferedImage image;
	private Graphics graphics;

	//KONSTRUKTOR Startzustand/Init
	GAME() {
		t = null;
		b = 1024;
		h = 800;
		time = (long) (System.currentTimeMillis()); 
		ti = null;
		neu = true;
		initPanel();

		image = new BufferedImage(b, h,BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();

		ziele = new LISTE();
		v = new VIERECK[80];
		bricks = new Rectangle[80];
		paddle = new VIERECK(550, 780, 110, 15);
		initGame();
		rendern();
		gameLoop();
	}

	// 1. PANNEL HERSTELLEN
	void initPanel() {
		setTitle("BREAKOUT-KLON");
		panel = (JPanel) getContentPane(); //Expliziter Typcast zum JPanel
		panel.setPreferredSize(new Dimension(b, h));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	// 2. Startzustand
	void initGame() {
		// Ball
		ball = new KREIS(400, 500, 6, 6, 30);
		// Targets in die Liste einfuegen
		for (int i = 0; i < 14; i++) {
			for (int j = 0; j < 4; j++) {
				v[i] = new VIERECK(100 + i * 60, 70 + j * 40, 50, 20);
				ziele.einfuegen(v[i]);
			}
		}
	}

	public void paddelBewegen() {
		if (links && paddle.getX() > 0) {
			// Paddle nach links
			paddle.setX(paddle.getX() - 10);
			viereckZeichnen(paddle.getX(), 780, paddle.getA(), paddle.getB());
		} else if (rechts && paddle.getX() < b - paddle.getA()) {
			// Paddle nach rechts
			paddle.setX(paddle.getX() + 10);
			viereckZeichnen(paddle.getX(), 780, paddle.getA(), paddle.getB());
		}
		// Paddle unver�ndert zeichnen
		viereckZeichnen(paddle.getX(), 780, paddle.getA(), paddle.getB());
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void ballBewegen(){
		r1 = new Rectangle(ball.getX(), ball.getY(), 30, 30);
		r2 = new Rectangle(paddle.getX(), paddle.getY(), paddle.getA(), paddle.getB());
		
		for(int i = 0; i < ziele.anzahlElementeGeben(); i++){
			bricks[i] = new Rectangle(((VIERECK) ziele.suchen(v[i])).getX(), ((VIERECK) ziele.suchen(v[i])).getY(), ((VIERECK) ziele.suchen(v[i])).getA(), ((VIERECK) ziele.suchen(v[i])).getB());
			viereckZeichnen(((VIERECK) ziele.suchen(v[i])).getX(), ((VIERECK) ziele.suchen(v[i])).getY(), ((VIERECK) ziele.suchen(v[i])).getA(), ((VIERECK) ziele.suchen(v[i])).getB());
		}
		//Kollissionen Bricks; ToDo Kollision mit x- oder y- Geraden
		for(int i = 0; i < ziele.anzahlElementeGeben(); i++){
			if(r1.intersects(bricks[i]) && !r1.intersects(r2)){
				ball.setVy(-ball.getVy());
				//System.out.println("HIT Brick: " + bricks[i]);
				//Brick zerstoeren
				bricks[i] = null;
				viereckZeichnen(((VIERECK) ziele.suchen(v[i])).getX(), ((VIERECK) ziele.suchen(v[i])).getY(), ((VIERECK) ziele.suchen(v[i])).getA(), ((VIERECK) ziele.suchen(v[i])).getB());
				ziele.loeschen(v[i]);
				break;
			}
		}
		if(ziele.anzahlElementeGeben()  == 0){
			//Alle Bricks zerst�oert
			long now = (long) (System.currentTimeMillis());
			String ti = (new Long(now - time).toString());
			graphics.drawString("Time: " + ti +"   Bricks left: " + (ziele.anzahlElementeGeben()), 400, 400);
			rendern();
			try {t.sleep(5000);} catch (InterruptedException e1) {}
			if(!neu){
				new GAME();
				neu = true;
			}
			t.interrupt();
		}
		//Kollission Wand
		if (ball.getX() <= 20) {
			ball.setVx(-ball.getVx());
		} else if (ball.getX() >= b - 20) {
			ball.setVx(-ball.getVx());
		}

		if (ball.getY() <= 0) {
			ball.setVy(-ball.getVy());
		} else if (ball.getY() > h - 50) {
			//Kollision Ball
			if (r1.intersects(r2)) {
				//Flugrichtung durch Paddlebewegung ver�ndern
				if(links){
					ball.setVy(-ball.getVy());
					if(ball.getVx() > - 10){
						ball.setVx(ball.getVx()-2);
					}ball.setVy(ball.getVy()-2);
				}
				else if(rechts){
					ball.setVy(-ball.getVy());
					if(ball.getVx() < 10){
						ball.setVx(ball.getVx() + 2);
					}ball.setVy(ball.getVy() + 2);
				}
				else if(!links && !rechts){
					ball.setVy(-ball.getVy());
				}
			}else{
				//Ball weg
				long now = (long) (System.currentTimeMillis());
				ti = (new Long(now - time).toString());
				graphics.drawString("Time: " + ti +"   Bricks left: " + (ziele.anzahlElementeGeben()), b/2 - 60, 400);
				graphics.drawString("Druecken Sie jetzt n, um ein neues Spiel zu beginnen", b/2 - 150, 450);
				rendern();
				//In die Datenbank schreiben
				DB d = new DB();
				int points = Integer.parseInt(ti) / ziele.anzahlElementeGeben();
				d.insertIntoDB(points);
				try {t.sleep(3000);} catch (InterruptedException e1) {}
				if(!neu){
					new GAME();
					neu = true;
				}else{
					hintergrundZeichnen();
					graphics.setColor(Color.CYAN);
					graphics.drawString("GAME OVER - HAVE A NICE DAY", b/2 - 100, h/2);
					rendern();
					t.interrupt();
					t.destroy();
				}
			}
		}
		ball.setX(ball.getX() + ball.getVx());
		ball.setY(ball.getY() + ball.getVy());
		kreisZeichnen(ball.getX() + ball.getVx(), ball.getY() + ball.getVy(), 30);
	}

	public void gameLoop() {
		t = new Thread("Game-Loop") {
			@SuppressWarnings("static-access")
			public void run() {
				while (!isInterrupted()) {
					// Zuweisen der Systemzeit in Millisekunden
					long nextFrameStart = System.currentTimeMillis();
					updateModell();
					nextFrameStart += FRAME_PERIOD;
					final long remaining = nextFrameStart - System.currentTimeMillis();
					/*
					 * Wenn das Rendern schneller ging, kurzen "sleep"
					 * durchf�hren
					 */
					if (remaining > 0) {
						try {t.sleep(10);} catch (final Throwable t) {}
					}
					/*
					 * else{}; ist unn�tig, da das Rendern ohnehin zu lang
					 * dauerte. Daraus resultiert ein Ruckeln (lags) im Spiel,
					 * da nicht alle Frames dargestellt wurden.
					 */
				}try {
					//Interrupted
					t.join();
				} catch (InterruptedException e) {}
			}
		};t.start();
	}

	// Alle Grafiken erstellen
	public void rendern() {
		final Graphics panelGraphics = panel.getGraphics();
		if (panelGraphics != null) {
			panelGraphics.drawImage(image, 0, 0, null);
			panelGraphics.dispose();
		}
	}

	// Update des Modells/Grafiken -> Spiellogik
	public void updateModell() {
		// Hintergrund neu darstellen
		hintergrundZeichnen();
		// Ball neu darstellen
		ballBewegen();
		// Paddel neu darstellen
		paddelBewegen();
		// Timer anzeigen
		long now = (long) (System.currentTimeMillis());
		ti = (new Long(now - time).toString());
		graphics.drawString("Time: " + ti +"   Bricks left: " + (ziele.anzahlElementeGeben()), 10, 10);
		// Rendern -> neues Bild erzeugen/aktuelles Spielbild
		rendern();
		graphics.clearRect(ball.getX(), ball.getY(), ball.getX(), ball.getY());
	}

	public final String getTi() {
		return ti;
	}

	public void kreisZeichnen(int x, int y, int d) {
		graphics.setColor(Color.RED);
		graphics.fillOval(x, y, d, d);
	}

	public void viereckZeichnen(int x, int y, int a, int b) {
		graphics.setColor(Color.GREEN);
		graphics.fillRect(x, y, a, b);
	}

	public void hintergrundZeichnen() {
		graphics.setColor(Color.BLACK);
		graphics.fillRect(0, 0, b, h);
	}

	// Feststellen von Benutzereingaben
	public void processKeyEvent(final KeyEvent e) {
		if (e.getKeyCode() == 37 && e.getID() == 401) {
			links = true;
			rechts = false;
			//System.out.println("Tastendruck links: " + KeyEvent.VK_LEFT +" Key ID: "+e.getID());

		} else if (e.getKeyCode() == 39 && e.getID() == 401) {
			rechts = true;
			links = false;
			//System.out.println("Tastendruck rechts: " + KeyEvent.VK_RIGHT +" Key ID: "+e.getID());

		} else if(e.getKeyCode() == 37 || e.getKeyCode() == 39 && e.getID() == 402){
			rechts = false;
			links = false;
			//System.out.println("Taste losgelassen");
		} else if (e.getKeyChar() == 'n' && e.getID() == 401){
			neu = false;
		}
	}
	public static void main(String[] args) {
		new GAME();
	}
}