/*
 * Adaption nach der Vorlage 
 * von Albert Wiedemann.
 * 
 */

class LISTE
{
    private LISTENELEMENT erstes;
    
    /**
     * Erzeugt eine leere Liste
     */
    LISTE ()
    {
        erstes = new ABSCHLUSS ();        
    }

    /**
     * Fuegt ein Objekt in die Liste ein.
     */
    void einfuegen (DATENELEMENT d)
    {
        erstes = erstes.einfuegen (d);
    }
    
    /**
     * Sucht das angegebene Objekt.
     */
    DATENELEMENT suchen (DATENELEMENT d)
    {
        return erstes.suchen (d);
    }

    /**
     * Loescht das angegebene Objekt.
     */
    void loeschen (DATENELEMENT d)
    {
        erstes = erstes.loeschen (d);
    }
    
    public int anzahlElementeGeben(){
        return erstes.anzahlDatenknotenGeben();
    }
}
