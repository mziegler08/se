class KREIS{
	public int x, y, vx, vy, d;
	
	public KREIS(int x, int y, int vx, int vy, int d){
		this.x = x;//Position
		this.y = y;
		this.vx = vx;//Bewegung
		this.vy = vy;
		this.d = d;//Durchmesser
	}

	public final int getX() {
		return x;
	}

	public final void setX(int x) {
		this.x = x;
	}

	public final int getY() {
		return y;
	}

	public final void setY(int y) {
		this.y = y;
	}

	public final int getVx() {
		return vx;
	}

	public final void setVx(int vx) {
		this.vx = vx;
	}

	public final int getVy() {
		return vy;
	}

	public final void setVy(int vy) {
		this.vy = vy;
	}

	public final int getD() {
		return d;
	}

	public final void setD(int d) {
		this.d = d;
	}

}