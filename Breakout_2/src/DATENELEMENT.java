abstract class DATENELEMENT
{
    /**
     * Vergleicht das Datemelement mit dem Parameter.
     * @param test Datenelement mit dem Testschl&uuml;ssel
     * @return true, wenn die Schl&uuml;ssel &uuml;bereinstimmen
     */
    abstract boolean istGleich (DATENELEMENT xyz);
}
