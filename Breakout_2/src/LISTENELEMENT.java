abstract class LISTENELEMENT
{
    LISTENELEMENT (){}

    abstract LISTENELEMENT einfuegen (DATENELEMENT d);
    abstract DATENELEMENT suchen (DATENELEMENT d);
    abstract LISTENELEMENT loeschen (DATENELEMENT d);
	abstract int anzahlDatenknotenGeben();
}
