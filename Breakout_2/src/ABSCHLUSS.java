class ABSCHLUSS extends LISTENELEMENT
{
    /**
     * Keine eigene Aktion
     */
    ABSCHLUSS ()
    {
        super ();
    }

    /**
     * @return (neues) Nachfolgeelement des Aufrufers.
     */
    LISTENELEMENT einfuegen (DATENELEMENT d)
    {
        return new KNOTEN (d, this);
    }
    
    /**
     * Sucht das angegebene Objekt.
     * @return Referenz auf das gefundene Objekt oder null.
     */
    DATENELEMENT suchen (DATENELEMENT d)
    {
        return null;
    }
    
    /**
     * @return (neues) Nachfolgeelement des Aufrufers.
     */
    LISTENELEMENT loeschen (DATENELEMENT d)
    {
        return this;
    }

    /**
     * Meldet die Anzahl der ab hier in der Liste befindlichen Datenelemente.
     * @return Anzahl
     */
    int anzahlAbHierGeben ()
    {
        return 0;
    }

	@Override
	public int anzahlDatenknotenGeben() {
		// TODO Auto-generated method stub
		return 0;
	}
}