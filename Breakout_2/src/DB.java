import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class DB {
	
	private Connection conn;
	private String url;
	private String dbName;
	private String driver;
	private String userName;
	private String password;
	
	//Hardcoded values
	public DB(){
		conn = null;
		url = "jdbc:mysql://db4free.net:3306/";
		dbName = "breakout";
		driver = "com.mysql.jdbc.Driver";
		userName = "m_ziegler";
		password = "hsg_za13";
	}
	//ToDo: Eingabemoeglichkeit eines Spielernamens
	public void insertIntoDB(int s){
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			//System.out.println("Connected to the database");
			//Statement
			Statement stmt = conn.createStatement();
			//Tabellenstruktur: id,time,name,score
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String uhrzeit = sdf.format(new Date());
			String insert = "INSERT INTO highscore VALUES(0,'"+uhrzeit+"','Player1',"+s+");";
			stmt.executeUpdate(insert);
			conn.close();
			//System.out.println("Disconnected from database");
		} catch (Exception e) {e.printStackTrace();}
	}
}