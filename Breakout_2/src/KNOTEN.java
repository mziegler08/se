class KNOTEN extends LISTENELEMENT
{
    private DATENELEMENT daten;
    private LISTENELEMENT nachfolger;

    /**
     * Baut die Referenzen auf
     * @param d Daten
     * @param nf Nachfolgeelement der Liste
     */
    KNOTEN (DATENELEMENT d, LISTENELEMENT nf)
    {
        super ();
        daten = d;
        nachfolger = nf;
    }

    /**
     * Objekt am Ende einfuegen.
     * @param d Referenz auf das einzuf&uuml;gende Objekt.
     * @return (neues) Nachfolgeelement des Aufrufers.
     */
    LISTENELEMENT einfuegen (DATENELEMENT d)
    {
        if (nachfolger != null)
        {
            nachfolger = nachfolger.einfuegen (d);
            return this;
        }
        else
        {
            return new KNOTEN (d, this);
        }
    }
    
    /**
     * Sucht das angegebene Objekt.
     * @param d Schluessel des zu suchenden Objekts.
     * @return Referenz auf das gefundene Objekt oder null.
     */
    DATENELEMENT suchen (DATENELEMENT d)
    {
        if (daten.istGleich(d))
        {
            return daten;
        }
        else if (!daten.istGleich(d))
        {
            return nachfolger.suchen (d);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Loescht das angegebene Objekt.
     * @param d Schluessel des zu l&ouml;schenden Objekts.
     * @return (neues) Nachfolgeelement des Aufrufers.
     */
    LISTENELEMENT loeschen (DATENELEMENT d)
    {
        if (daten.istGleich (d))
        {
            return nachfolger;
        }
        else if (!daten.istGleich (d))
        {
            return nachfolger.loeschen (d);
        }
        else
        {
            return this;
        }
    }
    public int anzahlDatenknotenGeben(){
        return 1 + nachfolger.anzahlDatenknotenGeben();
     }
}
