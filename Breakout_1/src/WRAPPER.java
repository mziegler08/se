import java.applet.Applet;

public class WRAPPER extends Applet {
	private static final long serialVersionUID = -285092474031256409L;
	private Thread t;
	
	public WRAPPER(){
		t = null;
	}
	
	public void start() {
      t = new Thread("Wrapper-Thread"){
          public void run() { 
        	  startApplication(); 
          }
       };t.start();
    }

    private void startApplication() {
       GAME.main(null);
    }
}