class VIERECK {
	public int a, b, x, y;

	public VIERECK(int x, int y, int a, int b) {
		this.a = a;
		this.b = b;
		this.x = x;
		this.y = y;
	}

	public final int getA() {
		return a;
	}

	public final void setA(int a) {
		this.a = a;
	}

	public final int getB() {
		return b;
	}

	public final void setB(int b) {
		this.b = b;
	}

	public final int getX() {
		return x;
	}

	public final void setX(int x) {
		this.x = x;
	}

	public final int getY() {
		return y;
	}

	public final void setY(int y) {
		this.y = y;
	}
}